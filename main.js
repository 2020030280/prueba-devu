//funciones//
function calcularEdad(fecha){
    const nacimiento = new Date(fecha);
    const fechaA = new Date();

    const diff = fechaA - nacimiento;

    const edad = Math.floor(diff/(1000*60*60*24*365));

    return edad;
}

function checarHistorial(rfc) {
    axios.get('./archivo.json')
      .then(response => {
        const data = response.data;
        if (Array.isArray(data)) {
          const tieneHistorial = data.some(elemento => {
            return elemento.RFC === rfc && elemento.Aprobado === 'Si';
          });
          return tieneHistorial;
        } else {
          console.error('El archivo JSON no contiene un array válido');
          throw new Error('El archivo JSON no contiene un array válido');
        }
      })
      .catch(error => {
        console.error('Error al obtener el archivo JSON:', error);
        throw error;
      });
  }


function SolicitarCredito(edad,Importe, Ingreso, rfc){
    let respuesta = document.getElementById('respuesta');
    
        const tieneHistorial = checarHistorial(rfc);
        
        if(edad>=18){
            let importeMaximo = 0;
            if(Ingreso >=5000 && Ingreso <10000){
                importeMaximo = (tieneHistorial) ? 15000 : 7500;
            }else if(Ingreso >=10000 && Ingreso <20000){
                importeMaximo = (tieneHistorial) ? 25000 : 12000;
            }else if(Ingreso >=20000 && Ingreso<40000){
                importeMaximo = (tieneHistorial) ? 50000 : 30000;
            }else if(Ingreso>=40000){
                importeMaximo = (tieneHistorial) ? 100000 : 50000;
            }else{
                respuesta.innerHTML = "La solicitud ha sido rechazada. No cuentas con los ingresos mensuales suficientes."
                return false;
            }
           if(Importe > importeMaximo){
            respuesta.innerHTML = "La solicitud ha sido rechazada. El importe maximo debe ser " + importeMaximo;
            return false;
           }else{
            respuesta.innerHTML = "La solicitud ha sido aprobada. Se aprobó un monto de " + Importe;
            return true;
           } 

     }else{

        respuesta.innerHTML = "La solicitud ha sido rechazada. El cliente es menor de edad."
        return false; 
     }
        
        
}


function ingresar(){
    let rfc = document.getElementById('RFC').value;
    let Nombre = document.getElementById('Nombre').value;
    let Fecha = document.getElementById('Fecha').value;
    let Importe = document.getElementById('Importe').value;
    let Ingreso = document.getElementById('Ingreso').value;
    if(rfc && Nombre && Fecha && Importe && Ingreso){
        const edad = calcularEdad(Fecha);
    
    
        SolicitarCredito(edad, Importe, Ingreso, rfc);
    
    }else{
        document.getElementById('respuesta').innerHTML = 'Rellena todos los datos';
    }
}